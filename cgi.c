/* Copyright (c) 2021, la-ninpre
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * *************************************************************************
 * 
 * this file contains code for cgi version of nimisewi.
 *
 * i know that there are libraries for cgi in c, but c'mon, it's so small and
 * simple.
 * stylesheet is arbitrary, replace with anything you want if you don't like it.
 */

#include <config.h>
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "nimisewi.h"

static void print_response_header(void);
static void print_html_header(void);
static void print_html_footer(void);
static void print_html_stylesheet(void);

static void
print_response_header(void)
{
    printf("Status: 200 OK\r");
    printf("Content-Type: text/html\r\r");
}

static void
print_html_header(void)
{
    printf("<!doctype html>\n");
    printf("<html>\n");
    printf("<head>\n");
    printf("<title>nimi sewi</title>\n");
    printf("<meta charset=\"utf-8\">\n");
    printf("<meta name=\"generator\" content=\"%s\">\n", PACKAGE_STRING);
    printf("<link rel=\"icon\" href=\"/assets/img/favicon.ico\" type=\"image/x-icon\">\n");
    print_html_stylesheet();
    printf("</head>\n");
    printf("<body>\n");
}

static void
print_html_stylesheet(void)
{
    printf("<style>\n");
    printf("body { background: #ff8; }\n");
    printf("h1 { text-align: center; font: bold 48pt monospace; color: #4ad }\n");
    printf("</style>\n");
}

static void
print_html_footer(void)
{
    printf("</body>\n");
    printf("</html>\n");
}

int 
main(void)
{
    char *ns;

#ifdef HAVE_PLEDGE
    if (pledge("stdio", NULL) == -1) {
        err(EXIT_FAILURE, "pledge");
    }
#endif

    print_response_header();
    print_html_header();

    printf("<h1>");
    ns = nimi_sewi(6);
    printf("%s", ns);
    weka_e_nimi_sewi(ns);
    printf("</h1>\n");


    print_html_footer();

    return EXIT_SUCCESS;
}
