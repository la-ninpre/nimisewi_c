#include <config.h>
#include <stdio.h>
#include <err.h>
#include <stdlib.h>
#include <unistd.h>

#include "nimisewi.h"

int
main(void)
{
    char *ns;

#ifdef HAVE_PLEDGE
    if (pledge("stdio", NULL) == -1) {
        err(EXIT_FAILURE, "pledge failed");
    }
#endif

    printf("20 text/gemini\r\n");
    printf("sina o lukin e nimi sewi:\n\n");
    ns = nimi_sewi(6);
    printf("# %s\n", ns);
    weka_e_nimi_sewi(ns);

    return EXIT_SUCCESS;
}
