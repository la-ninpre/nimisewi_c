# nimisewi

nimisewi is a small program that outputs random toki pona phrase.
[toki pona](https://tokipona.org) is a small conlang created by sonja lang.

[first implementation](https://fsl.aaoth.xyz/nimisewi.py)
was created using python, but then i decided to create more portable version.

## dependencies

- cmake (>=3.10)

## installation

installation is very straightforward:

```terminal
$ fsl clone https://fsl.aaoth.xyz/nimisewi.c
$ cd nimisewi
$ mkdir build && cd build
$ cmake ..
$ cmake --build .
$ doas make install
```

### compilation options

- **BUILD_CGI** -- compile `nimisewi.cgi`, a cgi version
  of nimisewi (default OFF).
  
    [see example](https://aaoth.xyz/nimisewi).

- **CGI_INSTALL_DIR** -- installation directory for `nimisewi.cgi`
  (default "$PREFIX/var/www/htdocs/cgi-bin").

- **BUILD_GEMINI_CGI** -- compile `nimisewi_gemini.cgi`, a cgi script
  for gemini protocol (default OFF).
  
    [see example](gemini://aaoth.xyz/cgi-bin/nimisewi).

- **GEMINI_CGI_INSTALL_DIR** -- installation directory for `nimisewi_gemini.cgi`
  (default "$PREFIX/var/gemini/cgi-bin").
  
to specify these options, pass them as `-D{OPTION}` to initial cmake
invocation. for example, these commands result in building both http and gemini
versions of cgi script.

``` terminal
$ mkdir build && cd build
$ cmake -DBUILD_CGI=ON -DBUILD_GEMINI=ON ..
$ cmake --build .
```

## usage

```terminal
$ nimisewi

musi taso jaki pi selo olin lukin
```

options:

- `-v` -- display version information and compilation features.
- `-i` -- display sad version of nimisewi prompt.

## adding new words

to add new words, edit `nimitoki.txt` file. add one word per line.

then, just clean everything and recompile the program.

```terminal
$ vim nimitoki.txt
$ cd build
$ make clean
$ make
  ...
```

## changelog

- **0.0.1** -- initial release
- **0.0.2** -- added license and comments to the code
- **0.0.3** -- added cgi for http and gemini, separated main code from
  nimisewi core
- **0.0.3.1** -- fixed errors in http header
- **0.0.4** -- added title and favicon to http cgi
- **0.0.4.1** -- fixed errors in http cgi
- **0.0.4.2** -- added missing error check to main
- **0.1.0** -- added words from lipu ku, added gnu autotools build system
- **0.1.1** -- added 'ku' word itself
- **0.2.0** -- switched to cmake build system, added `-v` and `--version`
  flags to nimisewi, that are outputting version information
  and compile features
- **0.3.0** -- added support for arc4random function if it exists on the system
- **0.3.1** -- added compiler name and version to `--version` flag of nimisewi
- **0.3.2** -- fixed link flags on linux
- **0.3.3** -- fixed compiler warnings and build system
- **0.4.0** -- added max number of words to nimisewi function, added new prompt
to the main program

## bugs

i am pretty much beginner in c, so it is possible that the code contains some
odd things.

if you have found something wrong or weird,
send me an email to `aaoth_at_aaoth_dot_xyz`.

## license

all code that makes up `nimisewi`, `nimisewi.cgi`, `nimisewi_gemini.cgi` and
`makenimitoki` programs is licensed under ISC license
(see [LICENSE](/file?name=LICENSE&ci=tip)).

`nimitoki.txt` is in public domain.

