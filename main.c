/* Copyright (c) 2021, la-ninpre
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * **********************************************************************
 *
 * this file contains main code for nimisewi as commandline utility
 * that throws it's phrases to stdout.
 *
 * should be obvious :)
 */

#include <config.h>
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "nimisewi.h"

#define NIMISEWI_MAMA 0x01
#define NIMISEWI_IKE  0x02

static void
pana_sona_pali(void)
{
    printf("nimisewi [-v|-i]\n");
    exit(EXIT_SUCCESS);
}

static void
pana_sona_mama(void)
{
    printf("%s\n", PACKAGE_STRING);
    printf("%s\n", PACKAGE_DESCRIPTION);
    printf("compiled on %s %s using %s (%ld-bit)\n",
            __DATE__, __TIME__, COMPILER_NAME, sizeof(void*)*8);
#ifdef HAVE_PLEDGE
    printf("HAVE_PLEDGE\n");
#endif
#ifdef HAVE_STRLCAT
    printf("HAVE_STRLCAT\n");
#endif
#ifdef HAVE_ARC4RANDOM_UNIFORM
    printf("HAVE_ARC4RANDOM_UNIFORM\n");
#endif
    /*return EXIT_SUCCESS;*/
}

static void
nimisewi_pona(void)
{
    char *ns;
    ns = nimi_sewi(6);
    printf("%s\n", ns);
    weka_e_nimi_sewi(ns);
}

static void
nimisewi_pi_pilin_ike(void)
{
    char *ns;
    ns = nimi_sewi(3);
    printf("%s mute li lon\n"
            "tan pi %s sin li seme?\n", ns, ns);
    weka_e_nimi_sewi(ns);
}

int
main(int argc, char *argv[])
{
    int ch, f;
#ifdef HAVE_PLEDGE
    if (pledge("stdio", NULL) == -1) {
        err(EXIT_FAILURE, "pledge");
    }
#endif
    f = 0;
    while ((ch = getopt(argc, argv, "vi")) != -1) {
        switch (ch) {
            case 'v':
                f |= NIMISEWI_MAMA;
                break;
            case 'i':
                f |= NIMISEWI_IKE;
                break;
            default:
                pana_sona_pali();
        }
    }
    argc -= optind;
    argv += optind;

    if (f & NIMISEWI_MAMA)
        pana_sona_mama();

    if (f & NIMISEWI_IKE)
        nimisewi_pi_pilin_ike();
    else
        nimisewi_pona();

    return EXIT_SUCCESS;
}

