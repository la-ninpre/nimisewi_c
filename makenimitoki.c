/* Copyright (c) 2021, la-ninpre
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * *************************************************************************
 * 
 * this file creates necessary data files for nimisewi.c.
 * 
 * i thought that it would be stupid to hardcode all the tokipona words into
 * actual code, so this little program is created. it reads nimitoki.txt file
 * which is in such format that one word is on separate line. i hope it's
 * easy now to add and remove desired words.
 * 
 * it's use is very narrow and tied to nimisewi.c, but it could be adapted.
 * 
 * TODO: make use of templates, maybe?
 */

#include <config.h>

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "makenimitoki.h"

void
make_header(FILE *nimitoki_h)
{
    fprintf(nimitoki_h,
            _MAKENIMITOKI_COMMENT_
            "#ifndef NIMI_TOKI_H\n"
            "#define NIMI_TOKI_H\n\n"

            "#include <stdlib.h>\n\n"

            "extern const char *nimi_toki[];    /* list of words */\n"
            "extern const unsigned long suli_pi_nimi_toki   /* length of word list */;\n\n"

            "#endif /* NIMI_TOKI_H */\n");
}

void
make_nimi_toki(FILE *nimitoki_txt, FILE *nimitoki_c)
{
    char buf[NIMI_TOKI_MAX_LENGTH];

    fprintf(nimitoki_c,
            _MAKENIMITOKI_COMMENT_
            "#include \"nimitoki.h\"\n\n");

    fprintf(nimitoki_c, "const char *nimi_toki[] = {\n");
    
    while (fgets(buf, NIMI_TOKI_MAX_LENGTH, nimitoki_txt) != NULL) {
        buf[strlen(buf)-1] = '\0';  /* i know it's weird, but :) */
        fprintf(nimitoki_c, "    \"%s\",\n", buf);
    }
    
    fprintf(nimitoki_c, "};\n\n");

    fprintf(nimitoki_c, "const unsigned long suli_pi_nimi_toki = "
                        "sizeof(nimi_toki)/sizeof(const char *);\n");
}

int
main(int argc, char *argv[])
{
    char *nimitoki_txt_path; /* file path for wordlist file */
    FILE *nimitoki_txt;      /* list of words */
    FILE *nimitoki_h;        /* output header file */
    FILE *nimitoki_c;        /* output code file */

#ifdef HAVE_PLEDGE
    if (pledge("stdio rpath wpath cpath", NULL) == -1) {
        err(EXIT_FAILURE, "pledge");
    }
#endif

    if (argc < 1 || argc > 2) {
        err(EXIT_FAILURE, "usage: makenimitoki <nimitoki.txt>");
    }

    nimitoki_txt_path = argv[1];

    nimitoki_txt = fopen(nimitoki_txt_path, "r");
    if (nimitoki_txt == NULL) {
        err(EXIT_FAILURE, "file not found");
    }

    nimitoki_h = fopen("nimitoki.h", "w");
    make_header(nimitoki_h);
    fclose(nimitoki_h);

    nimitoki_c = fopen("nimitoki.c", "w");
    make_nimi_toki(nimitoki_txt, nimitoki_c);
    fclose(nimitoki_txt);
    fclose(nimitoki_c);

    return 0;
}
